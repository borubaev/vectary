import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';
import 'package:webview_flutter_platform_interface/webview_flutter_platform_interface.dart';

class Plugin extends StatefulWidget {
  const Plugin({super.key});

  @override
  State<Plugin> createState() => _PluginState();
}

class _PluginState extends State<Plugin> {
  late final WebViewController _helicopter;
  late final WebViewController _elephant;
  late final WebViewController _cosmonavt;

  @override
  void initState() {
    late final PlatformWebViewControllerCreationParams params;
    if (WebViewPlatform.instance is WebKitWebViewPlatform) {
      params = WebKitWebViewControllerCreationParams(
        allowsInlineMediaPlayback: true,
        mediaTypesRequiringUserAction: const <PlaybackMediaTypes>{},
      );
    } else {
      params = const PlatformWebViewControllerCreationParams();
    }

    final WebViewController controller =
        WebViewController.fromPlatformCreationParams(params);
    // #enddocregion platform_features

    controller
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..loadRequest(
          Uri.parse('https://app.vectary.com/p/6h3VuYhNmGRmv1ASuaAiK8'));

    _helicopter = controller;

    final WebViewController controller2 =
        WebViewController.fromPlatformCreationParams(params);
    // #enddocregion platform_features

    controller2
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..loadRequest(
          Uri.parse('https://app.vectary.com/p/0zoubMGsvIxgzCOS6z9BId'));

    _elephant = controller2;
    final WebViewController controller3 =
        WebViewController.fromPlatformCreationParams(params);

    controller3
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..loadRequest(
          Uri.parse('https://app.vectary.com/p/5EV9QO3XLvZ6Kkj90AbF3r'));

    _cosmonavt = controller3;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '___borubaev__',
          style: Theme.of(context)
              .textTheme
              .headlineMedium!
              .copyWith(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: [
          Expanded(
            child: SizedBox(
              height: 100,
              child: WebViewWidget(controller: _helicopter),
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.bottomCenter,
              width: MediaQuery.of(context).size.width,
              height: 100,
              child: WebViewWidget(controller: _elephant),
            ),
          ),
          Expanded(
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 100,
              child: WebViewWidget(controller: _cosmonavt),
            ),
          ),
        ],
      ),
    );
  }
}
